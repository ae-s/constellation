%%%-------------------------------------------------------------------
%%% @author Astrid <astrid@obol-ecu2.xrtc.net>
%%% @copyright (C) 2018, Astrid
%%% @doc
%%%
%%% @end
%%% Created :  2 Jul 2018 by Astrid <astrid@obol-ecu2.xrtc.net>
%%%-------------------------------------------------------------------
-module(dg_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, add_device/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

add_device(Address) ->
    supervisor:start_child(?SERVER, [Address]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart intensity, and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->

    SupFlags = #{strategy => simple_one_for_one,
                 intensity => 100,
                 period => 5},

    Mgws = #{id => 'gateways',
             start => {mgw_sup, start_link, []},
             restart => transient,
             shutdown => 5000,
             type => worker,
             modules => [mgw_sup]},

    {ok, {SupFlags, [Mgws]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
