%%%-------------------------------------------------------------------
%%% @author Astrid <>
%%% @copyright (C) 2017, Astrid
%%% @doc
%%%
%%% Generic Call Agent server process, providing basic behavior for
%%% all media gateways.
%%%
%%% @end
%%% Created : 24 Oct 2017 by Astrid <>
%%%-------------------------------------------------------------------
-module(mgw_agent).

-include("include/mgcp.hrl").

-behaviour(gen_server).

%% API
-export([start_link/1]).

%% MGCP API
-export([notify/2
        ,restart_in_progress/2
         % below this line are forbidden messages
        ,endpoint_configuration/2
        ,notification_request/2
        ,create_connection/2
        ,modify_connection/2
        ,delete_connection/2
        ,audit_endpoint/2
        ,audit_connection/2
        ]).
-export([audit/3]).

%-export([new_endpoint/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {protocol
               ,domain
               ,localname
               ,events
               ,endpoints
               ,restart_method
               }).

%%%===================================================================
%%% API
%%%===================================================================

% 
notify(Address, Msg) ->
    gen_server:call({global, {agent, Address}}, Msg).

restart_in_progress(Address, Msg) ->
    gen_server:call({global, {agent, Address}}, Msg).

% messages forbidden in this direction
endpoint_configuration(_Address, _Msg) -> error.
notification_request(_Address, _Msg) -> error.
create_connection(_Address, _Msg) -> error.
modify_connection(_Address, _Msg) -> error.
delete_connection(_Address, _Msg) -> error.
audit_endpoint(_Address, _Msg) -> error.
audit_connection(_Address, _Msg) -> error.

%new_endpoint(Name, Address) ->
%    gen_server:cast({global, {agent, Address}}, {new_endpoint, Name, self()}).

audit(Address, EndpointID, Info) ->
    mgcp_protocol:audit_endpoint(Address, EndpointID, Info).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Address) ->
    gen_server:start_link({global, {agent, Address}}, ?MODULE, [Address], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([Address]) ->
    process_flag(trap_exit, true),
    {ok, #state{protocol = {protocol, Address}, endpoints = maps:new()}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({make_request, _Msg}, _From, State) ->
    % ??? what
    {reply, {error}, State};
handle_call(Msg = #mgcp_msg{topline = #mgcp_cmd{verb = restart_in_progress}}, _From, State) ->
    io:format("replying to restart-in-progress~n ~p~n", [Msg]),
    Phase = mgcp_parser:get_header(Msg, restart_method, undefined),
    io:format("phase ~p~n", [Phase]),
    Delay = mgcp_parser:get_header(Msg, restart_delay, 0),
    io:format("delay ~p~n", [Delay]),
    NewState = State#state{restart_method = Phase},
    io:format("newstate ~p~n", [NewState]),
    gen_server:cast(self(), {handle_restart_phase, Phase, Delay}),
    {reply, {ok, #mgcp_msg{topline = #mgcp_resp{code = 200, commentary = ""},
                           headers = [], body = <<>>}},
     NewState};
handle_call(Msg = #mgcp_msg{topline = #mgcp_cmd{verb = notify}}, _From, State) ->
    io:format("replying to notify~n ~p~n", [Msg]),
    {reply, {ok, #mgcp_msg{topline = #mgcp_resp{code = 200, commentary = ""},
                       headers = [], body = <<>>}},
     State};

handle_call(Msg = #mgcp_msg{}, From, State) ->
    io:format("mgw_agent at ~p:~n inbound command ~p~n", [State#state.protocol, Msg]),
    inbound(Msg#mgcp_msg.topline, Msg#mgcp_msg.headers, Msg#mgcp_msg.body, From, State);
%handle_call(Msg = #mgcp_msg{topline = #mgcp_cmd{verb = V, endpoint = {Local, Domain}}}, 
%            From, State) ->
%    {Reply, State1} =
%        case {V, Local} of
%            {restart_in, <<"*">>} -> 
%                {#mgcp_msg{topline = #mgcp_resp{code = 200, commentary = ""},
%                           headers = [], body = <<>>}, 
%                 State};
%            {_, <<"*">>} ->
%                {#mgcp_msg{topline = #mgcp_resp{code = 504, commentary = ""},
%                           headers = [], body = <<>>},
%                 State};
%            {_, _} ->
%                endpoint_msg({Local, Domain}, Msg, State)
%        end,
%    io:format("mgw_agent reply is ~p~n", [Reply]),
%    {reply, {ok, Reply}, State};
handle_call(Msg, From, State) ->
    io:format("mgw_agent at ~p: unhandled message ~p~n", [State#state.protocol, Msg]),
    {reply, {ok}, State}.


% TODO: call notify()
inbound(Cmd = #mgcp_cmd{endpoint = [<<"*">>, _]}, Headers, Body, From, State) ->
    io:format("mgw_agent at ~p: unhandled gateway command ~p~n", [State#state.protocol, Cmd]),
    {reply, {ok, #mgcp_msg{topline = #mgcp_resp{code = 504, commentary = ""},
                           headers = [], body = <<>>}},
     State};
inbound(Cmd = #mgcp_cmd{}, Headers, Body, From, State) ->
    % not caught by enpoint "*" above so it's a particular endpoint
    io:format("mgw_agent at ~p: endpoint command ~p~n", [State#state.protocol, Cmd]),
    endpoint_msg({Cmd#mgcp_cmd.endpoint}, #mgcp_msg{topline = Cmd, headers = Headers, body = Body}, State).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({ensure_endpoint_exists, Name, Pid}, State) ->
    {noreply,
     State#state{endpoints = maps:put(Name, endpoint, State#state.endpoints)}};
handle_cast({handle_restart_phase, Phase, Delay}, State) ->
    AuditInfo = mgcp_protocol:audit_endpoint(State#state.protocol, [""]),
    % XXX TODO
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%% have received a message from an endpoint, rather than from *
endpoint_msg({Name, Domain}, Msg, State) ->
    E = maps:get(Name, State#state.endpoints, none),
    {Endpoint, State2} = case E of
%                             none -> Emode = new, new_endpoint(Name);
                             E -> Emode = extant, E
                         end,
    {ok, Reply} = ep_agent:rsip(Endpoint, Msg),
    {reply, Reply, State}.

