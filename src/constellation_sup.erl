%%%-------------------------------------------------------------------
%%% @author Astrid <astrid@obol-ecu2.xrtc.net>
%%% @copyright (C) 2018, Astrid
%%% @doc
%%%
%%% @end
%%% Created :  2 Jul 2018 by Astrid <astrid@obol-ecu2.xrtc.net>
%%%-------------------------------------------------------------------
-module(constellation_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart intensity, and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->

    SupFlags = #{strategy => one_for_one,
                 intensity => 1,
                 period => 5},

    Router = #{id => 'udp_router',
               start => {'udp_router', start_link, [2727]},
               restart => permanent,
               shutdown => 5000,
               type => worker,
               intensity => 10,
               period => 2,
               modules => ['udp_router']},


    Device = #{id => 'dg_sup',
               start => {dg_sup, start_link, []},
               restart => permanent,
               shutdown => 5000,
               type => worker,
               modules => [dg_sup]},

    {ok, {SupFlags, [Router, Device]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
