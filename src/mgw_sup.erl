%%%-------------------------------------------------------------------
%%% @author Astrid <astrid@torx.6.xrtc.net>
%%% @copyright (C) 2018, Astrid
%%% @doc
%%%
%%% @end
%%% Created :  2 Jun 2018 by Astrid <astrid@torx.6.xrtc.net>
%%%-------------------------------------------------------------------
-module(mgw_sup).

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Address) ->
    supervisor:start_link(?MODULE, [Address]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([{Ip, Port}]) ->
    RestartStrategy = one_for_all,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Protocol = #{id => 'protocol'
                ,start => {mgcp_protocol, start_link, [{Ip, Port}]}
                ,restart => permanent
                ,type => worker},
    Agent = #{id => 'agent'
             ,start => {mgw_agent, start_link, [{Ip, Port}]}
             ,restart => permanent
             ,type => worker},
    EpSup = #{id => 'endpoints'
             ,start => {ep_sup, start_link, []}
             ,restart => permanent
             ,type => worker},
    {ok, {SupFlags, [Protocol, Agent, EpSup]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
