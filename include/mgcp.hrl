
-type endpoint() :: string().

-record(mgcp_cmd, {verb :: atom()
                  ,tid :: integer()
                  ,endpoint :: list(endpoint())
                  ,version :: atom()
                  }).

-record(mgcp_resp, {code :: integer()
                   ,package :: atom()
                   ,tid :: integer()
                   ,commentary :: binary()
                   }).

-record(mgcp_hdr, {name :: atom()
                  ,value :: binary() | list()
                  }).

-record(mgcp_msg, {topline :: #mgcp_cmd{} | #mgcp_resp{}
                  ,headers :: list(#mgcp_hdr{})
                  ,body :: binary()
                  }).

